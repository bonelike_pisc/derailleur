use image;
use texture::TextureSettings;
use opengl_graphics::GlyphCache;
use opengl_graphics::Texture as GlTexture;
use opengl_graphics::TextureSettings as GlTextureSettings;
use derailleur_draw::Font;
use derailleur_audio::sound::{SimpleSound, RepeatingSound};
use derailleur_util as util;

pub trait OnMemoryDecode<T> {
    fn decode(&self, bytes: Vec<u8>) -> Result<T, Box<dyn std::error::Error>>;
}

pub struct TTCFontDecoder {
    index: usize
}

impl TTCFontDecoder {
    pub fn new(index: usize) -> Self {
        Self { index: index }
    }
}

impl OnMemoryDecode<Font> for TTCFontDecoder {
    fn decode(&self, bytes: Vec<u8>) -> Result<Font, Box<dyn std::error::Error>> {
        let font_collection = rusttype::FontCollection::from_bytes(bytes)?;
        let font_iter = font_collection.into_fonts();
        let mut fonts = Vec::new();
        for font in font_iter {
            let g = GlyphCache::from_font(font.unwrap(), (), TextureSettings::new());
            fonts.push(Font::from_glyphcache(g));
        }
        if self.index >= fonts.len() {
            return Err(Box::new(util::error::IndexError::new(self.index, fonts.len())));
        }
        Ok(fonts.remove(self.index)) //先頭のフォントを使用する
    }
}

pub struct TextureDecoder {
    settings: GlTextureSettings
}

impl TextureDecoder {
    pub fn new() -> Self {
        Self {
            settings: GlTextureSettings::new()
        }
    }
}

impl OnMemoryDecode<GlTexture> for TextureDecoder {
    fn decode(&self, bytes: Vec<u8>) -> Result<GlTexture, Box<dyn std::error::Error>> {
        let rgba8 = image::load_from_memory(&bytes)
            .map(|i| i.to_rgba())?;

        Ok(GlTexture::from_image(&rgba8, &self.settings))
    }
}

pub struct SimpleSoundDecoder {}

impl SimpleSoundDecoder {
    pub fn new() -> Self {
        Self {}
    }
}

impl OnMemoryDecode<SimpleSound> for SimpleSoundDecoder {
    fn decode(&self, bytes: Vec<u8>) -> Result<SimpleSound, Box<dyn std::error::Error>> {
        let ret = SimpleSound::from_bytes(bytes)?;
        Ok(ret)
    }
}

pub struct RepeatingSoundDecoder {}

impl RepeatingSoundDecoder {
    pub fn new() -> Self {
        Self {}
    }
}

impl OnMemoryDecode<RepeatingSound> for RepeatingSoundDecoder {
    fn decode(&self, bytes: Vec<u8>) -> Result<RepeatingSound, Box<dyn std::error::Error>> {
        let ret = RepeatingSound::from_bytes(bytes)?;
        Ok(ret)
    }
}

pub struct UTF8TextDecoder {}

impl UTF8TextDecoder {
    pub fn new() -> Self {
        Self {}
    }
}

impl OnMemoryDecode<String> for UTF8TextDecoder {
    fn decode(&self, bytes: Vec<u8>) -> Result<String, Box<dyn std::error::Error>> {
        let s = std::str::from_utf8(bytes.as_slice())
            .map(|s| s.to_string())?;
        Ok(s)
    }
}

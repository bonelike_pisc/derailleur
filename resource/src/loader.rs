use std::io::{Read, BufReader};
use std::fs::File;
use super::OnMemoryDecode;
use derailleur_util::filter::{ReadFilter, SimpleReadFilter};

pub struct ResourceLoader<T, Rf, Rt>
    where Rf: Read, Rt: Read
{
    filter: Box<dyn ReadFilter<Rf, Rt>>,
    decoder: Box<dyn OnMemoryDecode<T>>,
}

impl <T, R> ResourceLoader<T, R, R> where R: Read {
    pub fn decoder<D>(decoder: D) -> Self 
        where D: OnMemoryDecode<T> + 'static
    {
        Self {
            filter: Box::new(SimpleReadFilter::new()),
            decoder: Box::new(decoder),
        }
    }

    pub fn filter<F, Rt>(self, filter: F) -> ResourceLoader<T, R, Rt>
        where
            F: ReadFilter<R, Rt> + 'static,
            Rt: Read
    {
        ResourceLoader {
            filter: Box::new(filter),
            decoder: self.decoder,
        }
    }
}

impl <T, R> ResourceLoader<T, BufReader<File>, R>
    where R: Read
{
    pub fn load<P: AsRef<std::path::Path>>(&self, path: P) -> Result<T, Box<dyn std::error::Error>> {
        let mut reader = File::open(path)
            .map(|f| BufReader::new(f))
            .map(|f| self.filter.filter(f))?;
        let mut data = Vec::new();
        reader.read_to_end(&mut data)?;
        let ret = self.decoder.decode(data)?;
        Ok(ret)
    }
}


use derailleur_draw::Font;
use opengl_graphics::Texture as GlTexture;
use derailleur_audio::sound::{SimpleSound, RepeatingSound};
//use super::{TTCFontDecoder, TextureDecoder, SimpleSoundDecoder, RepeatingSoundDecoder};

pub struct ResourceLoaderFactory {}

impl ResourceLoaderFactory {
    pub fn ttc() -> ResourceLoader<Font, BufReader<File>, BufReader<File>> {
        ResourceLoader::decoder(super::TTCFontDecoder::new(0))
    }

    pub fn texture() -> ResourceLoader<GlTexture, BufReader<File>, BufReader<File>> {
        ResourceLoader::decoder(super::TextureDecoder::new())
    }

    pub fn simple_sound() -> ResourceLoader<SimpleSound, BufReader<File>, BufReader<File>> {
        ResourceLoader::decoder(super::SimpleSoundDecoder::new())
    }

    pub fn repeating_sound() -> ResourceLoader<RepeatingSound, BufReader<File>, BufReader<File>> {
        ResourceLoader::decoder(super::RepeatingSoundDecoder::new())
    }

    pub fn text() -> ResourceLoader<String, BufReader<File>, BufReader<File>> {
        ResourceLoader::decoder(super::UTF8TextDecoder::new())
    }
}
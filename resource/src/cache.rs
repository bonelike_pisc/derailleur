use std::collections::HashMap;
use std::rc::Rc;
use std::clone::Clone;
use super::loader::ResourceLoader;

// 参照を複製して渡すタイプ
pub struct RcResourceCache<T> {
    map: HashMap<&'static str, Rc<T>>,
}

impl <T> RcResourceCache<T> {
    pub fn new() -> Self {
        Self {
            map: HashMap::new()
        }
    }

    pub fn load<P, R>(
        &mut self,
        key: &'static str,
        path: P,
        loader: ResourceLoader<T, std::io::BufReader<std::fs::File>, R>
    ) -> Result<(), Box<dyn std::error::Error>>
        where P: AsRef<std::path::Path>, R: std::io::Read
    {
        let obj = loader.load(path)?;
        self.map.insert(key, Rc::new(obj));
        Ok(())
    }

    pub fn get(&self, key: &'static str) -> Option<Rc<T>> {
        self.map.get(&key).map(|o| o.clone())
    }

    pub fn purge(&mut self, key: &'static str) {
        self.map.remove(key);
    }
}

// 実体を複製して渡すタイプ
pub struct ClonableResourceCache<T> {
    map: HashMap<&'static str, T>
}

impl <T: Clone> ClonableResourceCache<T> {
    pub fn new() -> Self {
        Self {
            map: HashMap::new()
        }
    }

    pub fn load<P, R>(
        &mut self,
        key: &'static str,
        path: P,
        loader: ResourceLoader<T, std::io::BufReader<std::fs::File>, R>
    ) -> Result<(), Box<dyn std::error::Error>>
        where P: AsRef<std::path::Path>, R: std::io::Read
    {
        let obj = loader.load(path)?;
        self.map.insert(key, obj);
        Ok(())
    }

    pub fn get(&self, key: &'static str) -> Option<T> {
        self.map.get(&key).map(|o| o.clone())
    }

    pub fn purge(&mut self, key: &'static str) {
        self.map.remove(key);
    }
}


pub trait Transition<T> {
    fn update(&self, trackee: &mut T, dt_sec: f64);
}



use derailleur_draw::feature::*;
use super::Transition;

pub struct LinearTracker {
    velo_x: f64, velo_y: f64
}

impl LinearTracker {
    pub fn new(velo_x: f64, velo_y: f64) -> Self {
        LinearTracker { velo_x: velo_x, velo_y: velo_y }
    }
}

impl <T> Transition<T> for LinearTracker where T: HasPosition {
    fn update(&self, trackee: &mut T, dt_sec: f64) {
        let next_pos = trackee.get_position().shifted(self.velo_x * dt_sec, self.velo_y * dt_sec);
        trackee.set_position(next_pos);
    }
}
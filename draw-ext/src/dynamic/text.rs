﻿use std::time::{Instant, Duration};
use std::rc::Rc;
use opengl_graphics::GlGraphics;
use derailleur_draw::{Draw, Text, Font};
use derailleur_draw::dynamic::DynamicDraw;

pub struct StretchingText {
    whole_str: String, //テキスト全量
    text: Text, //描画用テキストオブジェクト
    now_length: usize, //現在表示している文字数
    updated_time: Instant, //直前の伸長時刻
    stretching_cycle: Duration //伸長間隔
}

impl StretchingText {
    pub fn new(
        whole_str: &str,
        stretching_cycle_ms: u64,
        font_data: Rc<Font>,
        font_size: u32,
        position: [f64; 2],
        color: [f32; 4]
    ) -> Self {
        StretchingText {
            whole_str: String::from(whole_str),
            text: Text::new(
                "", font_data, font_size, position, color
            ),
            now_length: 0,
            updated_time: Instant::now(),
            stretching_cycle: Duration::from_millis(stretching_cycle_ms)
        }
    }
}

impl DynamicDraw for StretchingText {
    fn update(&mut self) {
        if self.now_length == self.whole_str.chars().count() {
            //すでに全文字表示しているときは何もしない
            //println!("{}", self.now_length);
            return
        }

        let elapsed = self.updated_time.elapsed();
        if elapsed.checked_sub(self.stretching_cycle).is_some() {
            //前回更新から更新間隔時間以上経過していた場合
            self.updated_time = Instant::now();
            self.now_length += 1;
            let mut new_str = String::new();
            let mut whole_chars = self.whole_str.chars();
            for _i in 0..self.now_length {
                new_str.push(whole_chars.next().unwrap());
            }

            self.text.set_str(&new_str);
        }
    }

    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics) {
        self.text.draw(c, gl);
    }
}

#[derive(Copy, Clone)]
pub struct Position {
    x: f64, y: f64
}

impl Position {
    pub fn new(x: f64, y: f64) -> Self {
        Position {x: x, y: y}
    }

    pub fn x(&self) -> f64 {self.x}
    pub fn y(&self) -> f64 {self.y}

    pub fn shifted(&self, shift_x: f64, shift_y: f64) -> Position {
        Position {
            x: self.x + shift_x,
            y: self.y + shift_y
        }
    }
}

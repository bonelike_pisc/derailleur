
#[derive(Copy, Clone)]
pub struct Angle {
    v_deg : f64
}

impl Angle {
    pub fn from_degree(v: f64) -> Angle {
        Angle{ v_deg: v }
    }

    pub fn from_radian(v: f64) -> Angle {
        Angle{ v_deg: v.to_degrees() }
    }

    pub fn as_degree(&self) -> f64 {
        self.v_deg
    }

    pub fn as_radian(&self) -> f64 {
        self.v_deg.to_radians()
    }

    pub fn sin(&self) -> f64 {
        self.v_deg.to_radians().sin()
    }

    pub fn cos(&self) -> f64 {
        self.v_deg.to_radians().cos()
    }

    pub fn tan(&self) -> f64 {
        self.v_deg.to_radians().tan()
    }

    pub fn add(a1: Angle, a2: Angle) -> Angle {
        Angle{ v_deg: a1.v_deg + a2.v_deg}
    }

    pub fn substract(a1: Angle, a2: Angle) -> Angle {
        Angle{ v_deg: a1.v_deg - a2.v_deg}
    }
}


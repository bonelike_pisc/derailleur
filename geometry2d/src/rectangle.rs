use super::position::Position;

#[derive(Copy, Clone)]
pub struct Rectangle {
    upper_left: Position,
    width: f64,
    height: f64
}

impl Rectangle {
    pub fn new(upper_left: Position, width: f64, height: f64) -> Self {
        Rectangle {
            upper_left: upper_left,
            width: width,
            height: height
        }
    }

    pub fn from_array(a: [f64; 4]) -> Self {
        Rectangle {
            upper_left: Position::new(a[0], a[1]),
            width: a[2],
            height: a[3]
        }
    }

    pub fn get_width(&self) -> f64 { self.width }
    pub fn get_height(&self) -> f64 { self.height }

    pub fn get_upper_left(&self) -> Position {
        self.upper_left
    }

    pub fn get_upper_right(&self) -> Position {
        self.upper_left.shifted(self.width, 0.0)
    }

    pub fn get_lower_left(&self) -> Position {
        self.upper_left.shifted(0.0, self.height)
    }

    pub fn get_lower_right(&self) -> Position {
        self.upper_left.shifted(self.width, self.height)
    }

    pub fn get_center(&self) -> Position {
        self.upper_left.shifted(self.width / 2.0, self.height / 2.0)
    }

    pub fn as_array(&self) -> [f64; 4] {
        [self.upper_left.x(), self.upper_left.y(), self.width, self.height]
    }
}

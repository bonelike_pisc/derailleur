use std::rc::Rc;
use std::cell::RefCell;
use piston::input;
use opengl_graphics::{GlGraphics, OpenGL};
use graphics;
use derailleur_draw::{Canvas, Draw};

pub trait Render {
    fn render(&mut self, args: &input::RenderArgs);
}

pub struct RcCanvasRenderer {
    canvas: Rc<RefCell<Canvas>>,
    bg_color: [f32; 4],
    gl: GlGraphics,
}

impl RcCanvasRenderer {
    pub fn new(c: Rc<RefCell<Canvas>>) -> Self {
        Self {
            canvas: c.clone(),
            bg_color: [0.0, 0.0, 0.0, 0.0],
            gl: GlGraphics::new(OpenGL::V3_2),
        }
    }
}

impl Render for RcCanvasRenderer {
    fn render(&mut self, args: &input::RenderArgs) {
        let canvas = self.canvas.borrow_mut(); //クロージャ内でself出てくると怒られるので…
        let bg_color = self.bg_color;
        self.gl.draw(args.viewport(), |c, gl| {
            graphics::clear(bg_color, gl);
            canvas.draw(c, gl);
        });
    }
}

pub struct RefCanvasRenderer<'a> {
    canvas: &'a Canvas,
    bg_color: [f32; 4],
    gl: GlGraphics,
}

impl <'a> RefCanvasRenderer<'a> {
    pub fn new(c: &'a Canvas) -> Self {
        Self {
            canvas: c,
            bg_color: [0.0, 0.0, 0.0, 0.0],
            gl: GlGraphics::new(OpenGL::V3_2),
        }
    }
}

impl <'a> Render for RefCanvasRenderer<'a> {
    fn render(&mut self, args: &input::RenderArgs) {
        let canvas = self.canvas;
        let bg_color = self.bg_color;
        self.gl.draw(args.viewport(), |c, gl| {
            graphics::clear(bg_color, gl);
            canvas.draw(c, gl);
        });
    }
}

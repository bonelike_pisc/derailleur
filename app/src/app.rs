use std::cell::RefCell;
use piston::input::*;
use crate::window;
use crate::render;
use crate::input::UIInput;
use crate::state::{State, StateChain, NextAction};

pub struct App<W, R> where W: window::Window, R: render::Render {
    window: RefCell<W>,
    renderer: RefCell<R>,
    state_chain: RefCell<StateChain>,
}

impl<W, R> App<W, R> where W: window::Window, R: render::Render {
	pub fn new(w: RefCell<W>, r: RefCell<R>, s: RefCell<StateChain>) -> App<W, R> {
		App {
			window: w,
			renderer: r,
			state_chain: s
		}
	}

    pub fn run(&mut self) {
        while let Some(e) = self.window.borrow_mut().next_event() {
            
            //画面の更新
            if let Some(r) = e.render_args() {
                self.renderer.borrow_mut().render(&r);
            }

            //状態の更新
            if let Some(u) = e.update_args() {
                let result = self.state_chain.borrow_mut().update(u.dt);
                //エラー発生または終了状態ならばループを抜ける
                if let Err(e) = result {
                    println!("{}", e);
                    break;
                }
                
                let next_action = self.state_chain.borrow().next_action();
                if let NextAction::Terminate = next_action {
                    break;
                }
            }

            //入力情報の適用
            if let Err(e) = self.state_chain.borrow_mut().on_input(UIInput::new(e)) {
                println!("{}", e);
                break;
            }
        }
    }
}
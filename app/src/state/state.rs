use std::error::Error;
use crate::input::UIInput;
use derailleur_util::error::SimpleError;

pub trait State {
    /// called once after transition from another state 
    fn initialize(&mut self) -> Result<(), StateRuntimeError> {
        Ok(())
    }

    /// called every time when an update event is handled
    ///
    /// dt_sec: duration (second) since last time the update event is handled 
    fn update(&mut self, _dt_sec: f64) -> Result<(), StateRuntimeError> {
        Ok(())
    }

    /// called every time when the input event is handled
    ///
    /// input: information about the input event (keyboard, mouse, etc...)
    fn on_input(&mut self, _input: UIInput) -> Result<(), StateRuntimeError> {
        Ok(())
    }

    /// called once before traisition to another state
    fn finalize(&mut self) -> Result<(), StateRuntimeError> {
        Ok(())
    }

    /// determine the next action
    fn next_action(&self) -> NextAction;

    /// called when next_action() returns ChangeState
    ///
    /// returns: the state object to which it is to change
    fn next_state(&self) -> Result<Box<dyn State>, StateRuntimeError> {
        Err(StateRuntimeError::from_message("[ERROR] not implemented."))
    }
}

pub enum NextAction {
    /// continue this state
    Continue,
    /// change to another state
    ChangeState,
    /// terminate the StateChain
    Terminate,
}

#[derive(Debug)]
pub struct StateRuntimeError {
    inner: Box<dyn Error>
}

impl StateRuntimeError {
    pub fn from_message(msg: &str) -> Self {
        Self {
            inner: Box::new(SimpleError::new(msg))
        }
    }

    pub fn from_another<E: 'static + Error>(err: E) -> Self {
        Self {
            inner: Box::new(err)
        }
    }
}

impl Error for StateRuntimeError {
    /* コンパイルできないので実装しない
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.inner)
    }
    */
}

impl std::fmt::Display for StateRuntimeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "StateRuntimeError. cause: {}", &self.inner)
    }
}
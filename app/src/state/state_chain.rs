use super::state::{State, StateRuntimeError, NextAction};
use crate::input::UIInput;

pub struct StateChain {
    now_state: Box<dyn State>,
    initialized: bool, //初期化済みフラグ
    terminated: bool, //終了済みフラグ
}

impl StateChain {
    pub fn new<T>(initial_state: T) -> Self
        where T: 'static + State
    {
        Self {
            now_state: Box::new(initial_state),
            initialized: false,
            terminated: false,
        }
    }
}

impl State for StateChain {
    // updateだけで制御するので、initialize, finalizeは実装しない。

    fn update(&mut self, dt_sec: f64) -> Result<(), StateRuntimeError> {
        if self.terminated { //終了済みの場合は何もしない
            //return Err(StateRuntimeError::from_message("state chain is already terminated."));
            return Ok(());
        }
        if !self.initialized { //初回または状態が切り替わった直後
            self.now_state.initialize()?;
            self.initialized = true;
        }

        self.now_state.update(dt_sec)?;
        let next_action = self.now_state.next_action();
        match next_action {
            NextAction::Continue => {}, //何もしない
            NextAction::ChangeState => {
                self.now_state.finalize()?;
                self.now_state = self.now_state.next_state()?;
                self.initialized = false;
            },
            NextAction::Terminate => {
                self.now_state.finalize()?;
                self.terminated = true;
            },
        }
        Ok(())
    }

    fn on_input(&mut self, input: UIInput) -> Result<(), StateRuntimeError> {
        if !self.initialized || self.terminated {
            return Ok(());
        }
        self.now_state.on_input(input)
    }

    fn next_action(&self) -> NextAction {
        let next_action = self.now_state.next_action();
        match next_action {
            NextAction::Continue => NextAction::Continue,
            NextAction::ChangeState => NextAction::Continue,
            NextAction::Terminate => NextAction::Terminate
        }
    }
}
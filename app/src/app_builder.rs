use std::rc::Rc;
use std::cell::RefCell;
use super::app;
use crate::window;
use crate::render;
use crate::state;
use derailleur_draw as draw;
use derailleur_global as global;

/*
 * Windowと初期State作成クロージャを受け取り、
 * Canvasに描画するAppを作成するショートカットメソッド。
 * Canvasは内部で作成し、クロージャで初期Stateに変換する。
 */
pub fn build_canvas_app<W, S>(
    window: RefCell<W>,
    initial_state_generator: &dyn Fn(Rc<RefCell<draw::Canvas>>) -> S
) -> app::App<W, render::RcCanvasRenderer>
    where W: window::Window, S: state::State + 'static
{
    let canvas = Rc::new(RefCell::new(draw::Canvas::new()));
    let renderer = RefCell::new(
		    render::RcCanvasRenderer::new(canvas.clone())
	);
    let initial_state = initial_state_generator(canvas);
    let state_chain = RefCell::new(
		    state::StateChain::new(initial_state)
    );

    app::App::new(window, renderer, state_chain)
}

pub fn build_singleton_app<'a, W, S>(window: RefCell<W>, initial_state: S)
    -> app::App<W, render::RefCanvasRenderer<'a>>
    where W: window::Window, S: state::State + 'static
{
    let canvas = global::main_canvas();
    let renderer = RefCell::new(
        render::RefCanvasRenderer::new(canvas)
    );
    let state_chain = RefCell::new(
        state::StateChain::new(initial_state)
    );

    app::App::new(window, renderer, state_chain)
}
use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow;

pub trait Window {
    fn next_event(&mut self) -> Option<Event>;
}

pub struct OpenGLWindow {
    window: GlutinWindow,
    event_queue: Events
}

impl OpenGLWindow {
    pub fn new(name: String, height: u32, width: u32) -> OpenGLWindow {

        let window: GlutinWindow = WindowSettings::new(
            name,
            [width, height]
        )
        .exit_on_esc(true)
        .build()
        .unwrap();

        OpenGLWindow {
            window: window,
            event_queue: Events::new(EventSettings::new())
        }
    }
}

impl Window for OpenGLWindow {
    fn next_event(&mut self) -> Option<Event> {
        self.event_queue.next(&mut self.window)
    }
}
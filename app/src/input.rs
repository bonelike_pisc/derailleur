use piston::input::Event;
use piston::input::PressEvent;
use piston::input::Button;
use piston::input::keyboard::Key;

pub struct UIInput {
    m_input: Event
}

impl UIInput {
    pub fn new(piston_input: Event) -> Self {
        Self {
            m_input: piston_input
        }
    }

    pub fn get(&self) -> &Event {
        &self.m_input
    }

    pub fn get_key_pressed(&self) -> Option<Key> {
        self.m_input.press_args()
        .and_then(|args| match args {
            Button::Keyboard(key) => Some(key),
            _ => None
        })
    }

    pub fn is_key_pressed(&self, key :Key) -> bool {
        if let Some(k) = self.get_key_pressed() {
            k == key
        }
        else {
            false
        }
    }
}
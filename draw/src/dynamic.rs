use std::cell::RefCell;
use opengl_graphics::GlGraphics;
use crate::draw::Draw;

pub trait DynamicDraw {
    fn update(&mut self);
    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics);
}

pub struct DynamicDrawAdaptor<T> where T: DynamicDraw {
    dd: RefCell<T>
}

impl <T> DynamicDrawAdaptor<T> where T: DynamicDraw {
    pub fn new(dd: T) -> Self {
        DynamicDrawAdaptor {
            dd: RefCell::new(dd)
        }
    }
}

impl <T> Draw for DynamicDrawAdaptor<T> where T: DynamicDraw {
    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics) {
        self.dd.borrow_mut().update();
        self.dd.borrow().draw(c, gl);
    }
}

pub fn make_draw<T>(dd: T) -> DynamicDrawAdaptor<T> where T: DynamicDraw {
    DynamicDrawAdaptor::new(dd)
}
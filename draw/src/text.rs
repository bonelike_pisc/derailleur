use std::cell::RefCell;
use std::cell::RefMut;
use rusttype;
use std::rc::Rc;
use opengl_graphics::GlGraphics;
use texture::TextureSettings;
use opengl_graphics::GlyphCache;
use super::Draw;

/**
 * 描画可能なテキスト
 */
pub struct Text {
    font: Rc<Font>,
    g_text_entity: graphics::Text,
    position: [f64; 2],
    draw_state: graphics::DrawState,
    text_str: String
}

pub struct Font {
    glyph_cache: RefCell<GlyphCache<'static>>
}

impl Text {
    pub fn new(
        text_str: &str,
        font_data: Rc<Font>,
        font_size: u32,
        position: [f64; 2],
        color: [f32; 4]
    ) -> Text {
        Text {
            font: font_data,
            g_text_entity: graphics::Text::new_color(color, font_size),
            position: position,
            draw_state: graphics::DrawState::default(),
            text_str: String::from(text_str)
        }
    }

    pub fn set_str(&mut self, s: &str) {
        self.text_str = String::from(s);
    }
}

impl Draw for Text {
    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics) {
        use std::ops::DerefMut;
        use graphics::Transformed;
        self.g_text_entity.draw(
            self.text_str.as_str(),
            self.font.get_glyph_cache().deref_mut(),
            &self.draw_state,
            c.transform.trans(self.position[0], self.position[1]),
            gl
        ).unwrap(); //失敗したらパニック
    }
}

impl Font {  
    /*
     * フォントファイル(.ttf)のパスを指定してインスタンス作成
     */
    pub fn from_ttf(
        font_path: &std::path::Path
    ) -> Result<Font, std::io::Error> {
        let g = GlyphCache::new(
            font_path,
            (), //Factory(指定不要)
            TextureSettings::new()
        )?;
        Ok(Font {
            glyph_cache: RefCell::new(g)
        })
    }

    pub fn from_ttc(
        font_path: &std::path::Path
    ) -> Result<Vec<Rc<Font>>, std::io::Error> {
        let mut ret = Vec::new();

        //フォントファイルを全量読み込み
        let mut file = std::fs::File::open(font_path)?;
        let mut file_content = Vec::new();
        use std::io::Read;
        file.read_to_end(&mut file_content)?;

        let font_collection = rusttype::FontCollection::from_bytes(file_content)?;
        let font_iter = font_collection.into_fonts();
        for font in font_iter {
            let g = GlyphCache::from_font(font.unwrap(), (), TextureSettings::new());
            ret.push(Rc::new(Font {
                glyph_cache: RefCell::new(g)
            }));
        }

        Ok(ret)
    }

    pub fn from_glyphcache(g: GlyphCache<'static>) -> Self {
        Self {
            glyph_cache: RefCell::new(g)
        }
    }

    /*
     * GlyphCacheを取得
     */
    pub fn get_glyph_cache(&self) -> RefMut<GlyphCache<'static>> {
        self.glyph_cache.borrow_mut()
    }
}
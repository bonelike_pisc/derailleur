use derailleur_geometry2d::{Position, Angle};

pub trait HasPosition {
    fn get_position(&self) -> Position;
    fn set_position(&mut self, p: Position); 
}

pub trait HasRotation {
    fn get_rotation(&self) -> Angle;
    fn set_rotation(&mut self, r: Angle);
}

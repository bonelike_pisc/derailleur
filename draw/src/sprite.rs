use std::rc::Rc;
use opengl_graphics::GlGraphics;
use opengl_graphics::Texture as GlTexture;
use texture::TextureSettings;
use std::ops::Deref;
use graphics::Transformed;
use derailleur_geometry2d::{Position, Rectangle, Angle};
use super::Draw;
use super::feature::{HasPosition, HasRotation};

/**
 * 画像スプライト
 */
pub struct Sprite {
    //image: graphics::Image,
    rect: Rectangle,
    gl_texture: Rc<GlTexture>,
    draw_state: graphics::DrawState,
    rotation: Angle
}

impl Sprite {
    /*
     * 画像ファイルを開いてスプライトを作成
     */
    pub fn from_path(path: &std::path::Path, rect: Rectangle, settings: &TextureSettings)
            -> Result<Self, String> {
        GlTexture::from_path(path, settings).map(|glt| {
            Self::from_gl_texture(Rc::new(glt), rect)
        })
    }

    /**
     * デフォルトのテクスチャ設定を用いてスプライトを作成
     */
    pub fn with_default_setting(path: &std::path::Path, rect: Rectangle)
            -> Result<Self, String> {
        Self::from_path(path, rect, &TextureSettings::new())
    }

    /**
     * 読み込み済みのテクスチャからスプライトを生成
     */
    pub fn from_gl_texture(texture: Rc<GlTexture>, rect: Rectangle)
            -> Self {
        Self {
            //image: graphics::Image::new().rect(rect),
            rect: rect,
            gl_texture: texture,
            draw_state: graphics::DrawState::default(),
            rotation: Angle::from_degree(0.0)
        }
    }

    /**
     * 描画時の設定
     */
    pub fn set_draw_state(&mut self, ds: graphics::DrawState) {
        self.draw_state = ds;
    }
}

impl HasPosition for Sprite {
    fn get_position(&self) -> Position {
        self.rect.get_upper_left()
    }

    fn set_position(&mut self, p: Position) {
        self.rect = Rectangle::new(p, self.rect.get_width(), self.rect.get_height());
    }
}

impl HasRotation for Sprite {
    fn get_rotation(&self) -> Angle {
        self.rotation
    }

    fn set_rotation(&mut self, r: Angle) {
        self.rotation = r;
    }
}

impl Draw for Sprite {
    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics) {
        let image = graphics::Image::new().rect(self.rect.as_array());
        image.draw(
            self.gl_texture.deref(), //参照外し
            &self.draw_state,
            c.rot_rad(self.rotation.as_radian()).transform,
            gl
        );
    }
}

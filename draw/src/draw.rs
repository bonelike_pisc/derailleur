use graphics;
use opengl_graphics::GlGraphics;

pub trait Draw {
    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics);
}

pub trait Lifetime {
    fn is_alive(&self) -> bool;
}

pub trait HasLifetime {
    fn get_lifetime(&self) -> &Box<dyn Lifetime>;
    fn set_lifetime(&mut self, lifetime: Box<dyn Lifetime>);
}

pub trait DrawWithLifetime: Draw + HasLifetime {}

pub struct StaticLifetime {
    value: bool
}

impl StaticLifetime {
    pub fn new(v: bool) -> StaticLifetime {
        StaticLifetime {value: v}
    }
}

impl Lifetime for StaticLifetime {
    fn is_alive(&self) -> bool {
        self.value
    }
}
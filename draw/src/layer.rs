use std::rc::Rc;
use std::rc::Weak;
use std::cell::RefCell;
use graphics;
use opengl_graphics::GlGraphics;
use super::draw::Draw;

/**
 * 描画オブジェクトを保持し、まとめて描画する。
 * 同じLayer内のオブジェクトの描画順序は未定義。
 */
pub struct Layer {
    //Drawの弱参照を保持
    drawees: Vec<Weak<RefCell<dyn Draw>>>
}

impl Layer {
    pub fn new() -> Layer {
        Layer { drawees: Vec::new() }
    }

    /**
     * Rcを受け取り、Weakに変換して追加
     */
    pub fn append_drawee(&mut self, d: Rc<RefCell<dyn Draw>>) {
        self.drawees.push(Rc::downgrade(&d));
    }

    /**
     * 参照先の実体が破棄された弱参照を削除
     */
    pub fn remove_dead_drawees(&mut self) {
        self.drawees.retain(|e|
            e.upgrade().is_some() //Someだけ残す（Noneは消す）
        );
    }

    pub fn is_empty(&self) -> bool {
        self.drawees.is_empty()
    }
}

impl Draw for Layer {
    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics) {
        for d in &self.drawees {
            match d.upgrade() {
                Some(r) => r.borrow().draw(c, gl),
                None => () //何もしない
            }
        }
    }
}

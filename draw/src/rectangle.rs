use graphics;
use opengl_graphics::GlGraphics;
use crate::draw::Draw;
use derailleur_geometry2d::Rectangle as GRectangle;

pub struct Rectangle {
    geometry: GRectangle,
    color: [f32; 4]
}

impl Rectangle {
    pub fn new(geometry: GRectangle, color: [f32; 4]) -> Self {
        Self {
            geometry: geometry,
            color: color
        }
    }
}

impl Draw for Rectangle {
    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics) {
        graphics::rectangle(self.color, self.geometry.as_array(), c.transform, gl);
    }
}

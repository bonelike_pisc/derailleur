use std::rc::Rc;
use std::cell::RefCell;
use graphics;
use opengl_graphics::GlGraphics;
use super::draw::Draw;
use super::layer::Layer;

pub struct Canvas {
    layers: Vec<Layer>
}

impl Canvas {
    pub fn new() -> Canvas {
        Canvas { layers: vec![Layer::new()] }
    }

    /**
     * Layerを背面側から0開始で数えてi番目の背面側に追加する。
     * i == self.layers.len()の場合は最前面に追加する。
     * i > self.layers.len()の場合は追加せずにfalseを返す。
     */
    pub fn insert_layer(&mut self, i: usize) -> bool {
        if i <= self.layers.len() {
            self.layers.insert(i, Layer::new());
            return true;
        }
        false
    }

    /**
     * 最前面にLayerを追加する。
     */
    pub fn append_front_layer(&mut self) {
        self.layers.push(Layer::new());
    }

    /**
     * i番目のLayerを取得する。
     * i >= self.layers.len()の場合はOption::None
     */
    pub fn get_layer(&mut self, i: usize) -> Option<&mut Layer> {
        self.layers.get_mut(i)
    } 

    /**
     * 最前面のLayerを取得する。
     * Layerが0個の場合はOption::None
     */
    pub fn get_front_layer(&mut self) -> Option<&mut Layer> {
        self.layers.last_mut()
    }

    /**
     * i番目のLayerにdraweeを追加するショートカットメソッド。
     * 対象Layerが無い場合は何もせずにfalseを返す。
     */
    pub fn append_drawee(&mut self, d: Rc<RefCell<dyn Draw>>, i: usize)
    -> bool {
        let layer = self.get_layer(i);
        match layer {
            Some(l) => {
                l.append_drawee(d);
                true
            },
            None => false
        }            
    }

    /// 最前面にLayerを追加してdraweeを追加するショートカットメソッド
    pub fn append_drawee_to_front(&mut self, d: Rc<RefCell<dyn Draw>>) {
        self.append_front_layer();
        let front = self.get_front_layer().unwrap();
        front.append_drawee(d);
    }

    /// 実体が破棄されたdraweeと空のLayerを削除
    pub fn gc(&mut self) {
        self.remove_dead_drawees();
        self.remove_empty_layers();
    }

    /**
     * 実体が破棄されたオブジェクトを削除
     */
    fn remove_dead_drawees(&mut self) {
        for l in &mut self.layers {
            l.remove_dead_drawees(); //破棄されたDrawへの参照を削除
        }
    }

    /**
     * 空のLayerを削除
     */
    fn remove_empty_layers(&mut self) {
        self.layers.retain(|e|
            !e.is_empty() //空でないLayerだけ残す
        );
    }

}

impl Draw for Canvas {
    fn draw(&self, c: graphics::Context, gl: &mut GlGraphics) {
        for l in &self.layers {
            l.draw(c, gl);
        }
    }
}

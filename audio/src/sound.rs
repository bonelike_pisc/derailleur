use std::io::Cursor;
use rodio::Source;

pub struct SimpleSound {
    buf: rodio::source::Buffered<rodio::Decoder<Cursor<Vec<u8>>>>
}

impl SimpleSound {
    pub fn from_bytes(data: Vec<u8>) -> Result<Self, rodio::decoder::DecoderError> {
        let cursor = Cursor::new(data);
        let source = rodio::Decoder::new(cursor)?;
        Ok(Self {
            buf: source.buffered()
        })
    }

    pub fn play(&self, device: &rodio::Device) {
        //let device = rodio::default_output_device().ok_or("no sound output device detected!")?;
        let sink = rodio::Sink::new(device);
        sink.append(self.buf.clone());
        sink.detach();
    }
}

pub struct RepeatingSound {
    buf: rodio::source::Repeat<rodio::Decoder<Cursor<Vec<u8>>>>,
}

impl RepeatingSound {
    pub fn from_bytes(data: Vec<u8>) -> Result<Self, rodio::decoder::DecoderError> {
        let cursor = Cursor::new(data);
        let source = rodio::Decoder::new(cursor)?;
        Ok(Self {
            buf: source.repeat_infinite()
        })
    }

    pub fn play(&self, device: &rodio::Device) -> rodio::Sink {
        let sink = rodio::Sink::new(device);
        sink.append(self.buf.clone());
        sink
    }
}


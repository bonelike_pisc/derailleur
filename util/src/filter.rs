use std::io::Read;
use super::crypt::Rc4DecodingReader;

pub trait ReadFilter<Rf, Rt> where Rf: Read, Rt: Read {
    fn filter(&self, from: Rf) -> Rt;
}

pub struct SimpleReadFilter {}

impl SimpleReadFilter {
    pub fn new() -> Self {
        Self {}
    }
}

impl <R> ReadFilter<R, R> for SimpleReadFilter where R: Read {
    fn filter(&self, from: R) -> R {
        from
    }
}

pub struct Rc4DecodingReadFilter<'a> {
    key: &'a [u8]
}

impl <'a> Rc4DecodingReadFilter<'a> {
    pub fn new(key: &'a [u8]) -> Self {
        Self {
            key: key
        }
    }
}

impl <'a, R> ReadFilter<R, Rc4DecodingReader<'a, R>> for Rc4DecodingReadFilter<'a>
    where R: Read {
    fn filter(&self, from: R) -> Rc4DecodingReader<'a, R> {
        Rc4DecodingReader::new(from, self.key)
    }
}
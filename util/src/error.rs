use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct SimpleError {
    msg: String
}

impl SimpleError {
    pub fn new(msg: &str) -> Self {
        Self {
            msg: msg.to_string()
        }
    }
}

impl Error for SimpleError {}

impl fmt::Display for SimpleError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.msg)
    }
}

#[derive(Debug)]
pub struct IndexError {
    attempted: usize,
    max_of_range: usize,
}

impl IndexError {
    pub fn new(attempted: usize, max_of_range: usize) -> Self {
        Self {
            attempted: attempted,
            max_of_range: max_of_range,
        }
    }
}

impl Error for IndexError {}

impl fmt::Display for IndexError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "index error! You attempted to access {}, but valid index value is less than {}",
                self.attempted, self.max_of_range)
    }
}

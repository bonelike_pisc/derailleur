use std::sync::Once;
use derailleur_resource::{RcResourceCache, ClonableResourceCache};
use opengl_graphics::Texture as GlTexture;
use derailleur_draw::{Font, Canvas};
use derailleur_audio::sound::{SimpleSound, RepeatingSound};

static RESOURCES_INIT: Once = Once::new();
static mut RESOURCES: Option<Resources> = Option::None;

/// 各種リソースデータのキャッシュにアクセス
pub fn get_resource<'a>() -> &'a mut Resources {
    unsafe {
        // 初回実行時のみ、初期化を行う
        RESOURCES_INIT.call_once(||
            RESOURCES = Some(Resources::new())
        );
        RESOURCES.as_mut().unwrap()
    }
}

pub struct Resources {
    pub fonts: RcResourceCache<Font>,
    pub textures: RcResourceCache<GlTexture>,
    pub texts: ClonableResourceCache<String>,
    pub simple_sounds: RcResourceCache<SimpleSound>,
    pub repeating_sounds: RcResourceCache<RepeatingSound>,
}

impl Resources {
    fn new() -> Self {
        Self {
            fonts: RcResourceCache::new(),
            textures: RcResourceCache::new(),
            texts: ClonableResourceCache::new(),
            simple_sounds: RcResourceCache::new(),
            repeating_sounds: RcResourceCache::new(),
        }
    }
}

static MAIN_CANVAS_INIT : Once = Once::new();
static mut MAIN_CANVAS : Option<Canvas> = Option::None;

/// グローバルスコープのCanvasにアクセス
pub fn main_canvas<'a>() -> &'a mut Canvas {
    unsafe {
        MAIN_CANVAS_INIT.call_once(||
            MAIN_CANVAS = Some(Canvas::new())
        );
        MAIN_CANVAS.as_mut().unwrap()
    }
}

pub fn get_sound_device() -> Option<rodio::Device> {
    rodio::default_output_device()
}